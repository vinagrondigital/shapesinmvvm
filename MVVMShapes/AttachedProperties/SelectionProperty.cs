﻿using MVVMShapes.Models;
using System.Windows;
using System.Windows.Input;

namespace MVVMShapes.AttachedProperties
{
    /// <summary>
    /// Enables selection for a DesignerItemModelBase item
    /// </summary>
    public static class SelectionProperty
    {
        #region Dependency properties
        public static readonly DependencyProperty EnabledForSelectionProperty =
            DependencyProperty.RegisterAttached("EnabledForSelection", typeof(bool), typeof(SelectionProperty),
                new FrameworkPropertyMetadata(false,
                    new PropertyChangedCallback(OnEnabledForSelectionChanged)));

        /// <summary>
        /// Get the selectionEnabled value
        /// </summary>
        /// <param name="d">dependency object to get the value from</param>
        /// <returns></returns>
        public static bool GetEnabledForSelection(DependencyObject d)
        {
            return (bool)d.GetValue(EnabledForSelectionProperty);
        }

        /// <summary>
        /// Set the selectionEnabled value
        /// </summary>
        /// <param name="d">dependency object to set the value</param>
        /// <param name="value">selection enabled value</param>
        public static void SetEnabledForSelection(DependencyObject d, bool value)
        {
            d.SetValue(EnabledForSelectionProperty, value);
        }

        /// <summary>
        /// Callback for hooking the PreviewMouseDown event
        /// </summary>
        /// <param name="d">dependency object</param>
        /// <param name="e">arg parameters</param>        
        private static void OnEnabledForSelectionChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FrameworkElement fe = (FrameworkElement)d;
            if ((bool)e.NewValue)
            {
                fe.PreviewMouseDown += PreviewMouseDown;
            }
            else
            {
                fe.PreviewMouseDown -= PreviewMouseDown;
            }
        } 
        #endregion

        /// <summary>
        /// Mouse event hook method
        /// </summary>
        /// <param name="sender">framework elemnt</param>
        /// <param name="e">mouse event args</param>
        private static void PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (sender is FrameworkElement fe && fe.DataContext is DesignerItemModelBase item)
            {
                if (item.IsSelected == false)
                {
                    if (item.Parent != null && item.Parent.Items != null)
                    {
                        if (!(Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl)))
                        {
                            item.Parent.DeselectAllItems();
                        }
                    }

                    //select the current item
                    item.Select();
                }
            }
        }
    }
}
