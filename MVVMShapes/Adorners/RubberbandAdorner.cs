﻿using MVVMShapes.Controls;
using MVVMShapes.Interfaces;
using MVVMShapes.Models;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace MVVMShapes.Adorners
{
    /// <summary>
    /// RubberbandAdorner based from https://github.com/microsoft/WPF-Samples/blob/master/Sample%20Applications/PhotoStoreDemo/RubberbandAdorner.cs
    /// Handles rubberband selection via the WPF adorner layer
    /// </summary>
    public class RubberbandAdorner : Adorner
    {
        #region Private data
        private Point? _startPoint;
        private Point? _endPoint;
        private readonly Pen _pen;
        private DesignerCanvas _designCanvas; 
        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="designerCanvas">iDEA DesignerCanvas reference</param>
        /// <param name="dragStartPoint">Mouse drag start point</param>
        public RubberbandAdorner(DesignerCanvas designerCanvas, Point? dragStartPoint) : base(designerCanvas)
        {
            _designCanvas = designerCanvas;
            _startPoint = dragStartPoint;

            //red for now...
            _pen = new Pen(Brushes.Red, 1);
        }

        #region Mouse event overrides
        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (IsMouseCaptured == false)
                {
                    CaptureMouse();
                }

                //get the end position
                _endPoint = e.GetPosition(this);

                //manage item selction
                UpdateSelection();

                //Invalidates the rendering of the element, and forces a complete new layout pass.OnRender(DrawingContext) is called after the layout cycle is completed.
                InvalidateVisual();
            }
            else
            {
                if (IsMouseCaptured)
                {
                    ReleaseMouseCapture();
                }
            }

            e.Handled = true;
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);

            //release the mouse capture (if captured)
            if (IsMouseCaptured)
            {
                ReleaseMouseCapture();
            }

            //remove this adorner from the adorner layer for the UIElement
            AdornerLayer layer = AdornerLayer.GetAdornerLayer(_designCanvas);

            if (layer != null)
            {
                layer.Remove(this);
            }

            e.Handled = true;
        }
        #endregion

        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            // without a background the OnMouseMove event would not be fired !
            // Alternative: implement a Canvas as a child of this adorner, like
            // the ConnectionAdorner does.
            drawingContext.DrawRectangle(Brushes.Transparent, null, new Rect(RenderSize));

            if (_startPoint.HasValue && _endPoint.HasValue)
            {
                drawingContext.DrawRectangle(Brushes.Transparent, _pen, new Rect(_startPoint.Value, _endPoint.Value));
            }
        }

        private T GetParent<T>(Type parentType, DependencyObject dependencyObject) where T : DependencyObject
        {
            DependencyObject parent = VisualTreeHelper.GetParent(dependencyObject);
            if (parent.GetType() == parentType)
                return (T)parent;

            return GetParent<T>(parentType, parent);
        }


        /// <summary>
        /// Selects all the items within the bounds of the rectangle
        /// </summary>
        private void UpdateSelection()
        {            
            if (_designCanvas.DataContext is IEditorDocument vm && vm.ImgEditor != null)
            {
                Rect rubberBand = new Rect(_startPoint.Value, _endPoint.Value);
                ItemsControl itemsControl = GetParent<ItemsControl>(typeof(ItemsControl), _designCanvas);

                foreach(DesignerItemModelBase item in vm.ImgEditor.Items)
                {
                    DependencyObject container = itemsControl.ItemContainerGenerator.ContainerFromItem(item);

                    Rect itemRect = VisualTreeHelper.GetDescendantBounds((Visual)container);
                    Rect itemBounds = ((Visual)container).TransformToAncestor(_designCanvas).TransformBounds(itemRect);

                    if (rubberBand.Contains(itemBounds))
                    {
                        item.Select();                        
                    }
                    else
                    {
                        if (!(Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl)))
                        {                            
                            item.Deselect();
                        }
                    }
                }
            }
        }
    }
}
