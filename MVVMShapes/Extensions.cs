﻿using System;

namespace MVVMShapes.Extensions
{
    // TODO: Add this to another extension in iDEALibraries.Extensions => GeneralExtensions class
    public static class GeneralExtensions
    {
        public static Tout When<Tin, Tout>(this Tin value, bool predicate, Func<Tin, Tout> success, Func<Tin, Tout> fail)
        {
            return predicate ? success(value) : fail(value);
        }
    }
}
