﻿using MVVMShapes.Editors;
using MVVMShapes.Interfaces;
using MVVMShapes.Models;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.IO;

namespace MVVMShapes.ViewModels
{
    public class MainWindowViewModel : BindableBase, IEditorDocument
    {
        #region VM Bindable properties
        private string _title = "Prism Application v1.0";
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private ImageEditor _imgEditor;
        public ImageEditor ImgEditor
        {
            get { return _imgEditor; }
            set { SetProperty(ref _imgEditor, value); }
        }

        private string _CenterX;
        public string CenterX
        {
            get { return _CenterX; }
            set { SetProperty(ref _CenterX, value); }
        }

        private string _centerY;
        public string CenterY
        {
            get { return _centerY; }
            set { SetProperty(ref _centerY, value); }
        }

        private string _R1;
        public string R1
        {
            get { return _R1; }
            set { SetProperty(ref _R1, value); }
        }

        private string _r2;
        public string R2
        {
            get { return _r2; }
            set { SetProperty(ref _r2, value); }
        }

        private string _ts;
        public string TS
        {
            get { return _ts; }
            set { SetProperty(ref _ts, value); }
        }

        private string _t2;
        public string TE
        {
            get { return _t2; }
            set { SetProperty(ref _t2, value); }
        }
        #endregion

        #region DelegateCommands
        public DelegateCommand CreateNewCommand { get; set; }
        public DelegateCommand CreateNewCommand2 { get; set; }
        public DelegateCommand SelectAllCommand { get; set; }
        public DelegateCommand DeselectAllCommand { get; set; }
        public DelegateCommand DeleteItemCommand { get; set; }
        public DelegateCommand DeleteAllItemsCommand { get; set; }
        #endregion

        #region VM Private data
        private List<DBCoordinate> dbRectCoord1;
        #endregion

        public MainWindowViewModel()
        {
            //temporary
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Resources");
            ImgEditor = new ImageEditor(Path.Combine(path, "eol.jpg"));

            dbRectCoord1 = new List<DBCoordinate>()
            {
                new DBCoordinate()
                {
                    X = 300,
                    Y = 300,
                },

                new DBCoordinate()
                {
                    X = 400,
                    Y = 400,
                },
            };


            CenterX = "0";
            CenterY = "0";
            R1 = "25";
            R2 = "50";
            TS = "0";
            TE = "180";

            CreateNewCommand = new DelegateCommand(() => ImgEditor.CreateItem(dbRectCoord1, DesignerItemType.RectangularROI));
            CreateNewCommand2 = new DelegateCommand(CreateAngleROI);
            SelectAllCommand = new DelegateCommand(() => ImgEditor.SelectAllItems());
            DeselectAllCommand = new DelegateCommand(() => ImgEditor.DeselectAllItems());
            DeleteItemCommand = new DelegateCommand(() => ImgEditor.DeleteAllItems(true));
            DeleteAllItemsCommand = new DelegateCommand(() => ImgEditor.DeleteAllItems());
        }

        private void CreateAngleROI()
        {
            try
            {
                List<DBCoordinate> dbPointerAngleCoord1 = new List<DBCoordinate>()
                {
                    new DBCoordinate()
                    {
                        X = double.Parse(CenterX),
                        Y = double.Parse(CenterY),
                        R1 = double.Parse(R1),
                        R2 = double.Parse(R2),
                        Theta_start = double.Parse(TS),
                        Theta_end = double.Parse(TE),
                    },
                };

                ImgEditor.CreateItem(dbPointerAngleCoord1, DesignerItemType.PointerAngleROI);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error {ex.Message}");
            }
        }
    }
}
