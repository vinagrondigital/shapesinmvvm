﻿using System;
using System.Linq;
using System.Windows;
using System.Collections.Generic;

using MVVMShapes.Models;
using MVVMShapes.Extensions;

namespace MVVMShapes.Utilities
{
    public static class ArcUtilities
    {
        public static double GetPointerAngleLimit(PointerAngleCoordinate arcDefinition, IEnumerable<Point> absPoints, Limit selectedLimit)
        {
            //parameters to be filled
            double theta;
            Axis selectedAxis;
            bool isMinBoundary;

            if (arcDefinition == null)
            {
                throw new Exception("Could not get arc boundary, arc point angle definition is null.");
            }

            if (absPoints == null || absPoints.Count() == 0)
            {
                throw new ArgumentException("Could not get arc boundary, absolute points are empty.");
            }

            switch (selectedLimit)
            {
                case Limit.Top:
                    theta = 90;
                    selectedAxis = Axis.Y;
                    isMinBoundary = true;
                    break;
                case Limit.Left:
                    theta = 180;
                    selectedAxis = Axis.X;
                    isMinBoundary = true;
                    break;
                case Limit.Right:
                    theta = 0;
                    selectedAxis = Axis.X;
                    isMinBoundary = false;
                    break;
                case Limit.Bottom:
                    theta = 270;
                    selectedAxis = Axis.Y;
                    isMinBoundary = false;
                    break;
                default:
                    throw new Exception($"Limit \"{selectedLimit}\" is not valid.");
            }

            return GetLimit(arcDefinition, absPoints, theta, selectedAxis, isMinBoundary);

        }

        private static double GetLimit(PointerAngleCoordinate arcDefinition, IEnumerable<Point> definitionPoins, double theta, Axis selectedAxis, bool isMinBoundary)
        {
            //if the arc contains the angle described, get the greater point by substracting the outer radius to the roi center
            if (GeometryUtilities.IsArcInRange(theta, arcDefinition.ThetaStart, arcDefinition.ThetaEnd))
            {
                return ((selectedAxis == Axis.X) ? arcDefinition.Center.X :                                              // use the value point relative to the axis
                                                   arcDefinition.Center.Y)
                                                   .When(isMinBoundary, (value) => (value - arcDefinition.OuterRadius),  // substract radius when limit is a minimum 
                                                                        (value) => (value + arcDefinition.OuterRadius)); // add radius when limit is a maximum
            }

            // the arc does not contain the boundary absolute point. Get the boundary nearest point
            return definitionPoins.When(selectedAxis == Axis.X, (points) => points.Select(p => p.X),    // get all the points values relative to the axis
                                                                (points) => points.Select(p => p.Y))
                                  .When(isMinBoundary, (values) => values.Min(),                        // find the point nearest to the minimum or maximum limit
                                                       (values) => values.Max());
        }
    }
}
