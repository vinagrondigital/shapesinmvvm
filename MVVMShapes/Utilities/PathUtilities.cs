﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Collections.Generic;

using MVVMShapes.Models;
using MVVMShapes.SearchZones;
using MVVMShapes.Interfaces;

namespace MVVMShapes.Utilities
{
    public static class PathUtilities
    {
        #region Private Data
        private static readonly double _maxLineLength  = 3;
        private static readonly double _linePercentage = 5; 
        #endregion

        /// <summary>
        ///     Creates the geometry of the center cross formed by two lines.
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        public static GeometryGroup CreateZoneGeometry<T>(T designeritem) where T : Zone
        {
            if(designeritem == null)    
            {
                throw new ArgumentNullException(nameof(designeritem));
            }

            GeometryGroup group = new GeometryGroup();

            switch(designeritem)
            {
                case RectangularZone recZone:
                    group.Children.Add(new RectangleGeometry(CreateRectangleGeometry(recZone)));
                    group.Children.Add(new PathGeometry(CreateCenterCrossPath(recZone.ItemWidth, recZone.ItemHeight)));
                    break;
                case PointerAngleZone pointerAngleZone:
                    group.Children.Add(new PathGeometry(CreatePointerAngleGeometry(pointerAngleZone)));
                    break;
                default:
                    throw new NotImplementedException();
            }

            return group;
        }

        private static Rect CreateRectangleGeometry(RectangularZone zone)
        {
            Rect rectangle = new Rect(new Point(0, 0), new Size(zone.ItemWidth, zone.ItemHeight));

            //TODO: is there something to do here?

            return rectangle;
        }

        private static List<PathFigure> CreatePointerAngleGeometry(PointerAngleZone zone)
        {
            List<PathFigure> pathFigure = BuildPointerAnglePath(zone);

            //add cross center and middle arc cross path figures
            pathFigure.AddRange(CreateCenterCrossPath(zone.CenterCoord));
            pathFigure.AddRange(CreateMiddleArcCrossPath(zone.CenterCoord));

            return pathFigure;
        }

        private static List<PathFigure> BuildPointerAnglePath(PointerAngleZone zone)
        {
            bool isBigArc = (zone.CenterCoord.ThetaStart > zone.CenterCoord.ThetaEnd) ? ((360 - (zone.CenterCoord.ThetaStart - zone.CenterCoord.ThetaEnd)) > 180) : (zone.CenterCoord.ThetaEnd - zone.CenterCoord.ThetaStart) > 180;
            Point start = zone.MyPoints[0];

            List<PathSegment> mainSegments = new List<PathSegment>
            {
                new ArcSegment (zone.MyPoints[1], new Size(zone.CenterCoord.InnerRadius, zone.CenterCoord.InnerRadius), rotationAngle:0, isBigArc, SweepDirection.Counterclockwise, true),
                new LineSegment(zone.MyPoints[2], true),
                new ArcSegment (zone.MyPoints[3], new Size(zone.CenterCoord.OuterRadius, zone.CenterCoord.OuterRadius), rotationAngle:0, isBigArc, SweepDirection.Clockwise, true),
            };

            return new List<PathFigure> { new PathFigure(start, mainSegments, true) };
        }

        /// <summary>
        ///     Creates a two line formed center cross (one vertical and the other horizontal).
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        public static List<PathFigure> CreateCenterCrossPath(double width, double height)
        {
            double deltaHor = ApplyPercentage(width,  _linePercentage);
            double deltaVer = ApplyPercentage(height,  _linePercentage);

            if (deltaHor > _maxLineLength)
            {
                deltaHor = _maxLineLength;
            }

            if (deltaVer > _maxLineLength)
            {
                deltaVer = _maxLineLength;
            }

            deltaHor = deltaVer = Math.Min(deltaVer, deltaHor);


            return new List<PathFigure>
            {
                //hor
                CreateCenterLineOnBox(width, height, deltaHor, 0).ToPathFigure(),

                //ver
                CreateCenterLineOnBox(width, height, 0, deltaVer).ToPathFigure(),
            };
        }

        /// <summary>
        ///     Creates a two line center cross given a pointer angle zone in the arc point center.
        /// </summary>
        /// <param name="angleROI"></param>
        /// <returns></returns>
        public static List<PathFigure> CreateCenterCrossPath(PointerAngleCoordinate angleROI)
        {
            double delta = ApplyPercentage(angleROI.OuterRadius, _linePercentage);

            if (delta > _maxLineLength)
            {
                delta = _maxLineLength;
            }

            return new List<PathFigure>
            {
                //hor
                CreateCenterLineOnPoint(angleROI.Center, delta, 0).ToPathFigure(),

                //ver
                CreateCenterLineOnPoint(angleROI.Center, 0, delta).ToPathFigure(),
            };
        }

        /// <summary>
        ///     Creates a two line formed center cross inside the arc formed by the pointer angle zone.
        ///     e.g for an arc with (r1, r2) => (10, 30) and (theta1, theta2) => (0, 180), the cross will 
        ///     be placed in the coord (r, theta) => (20, 90)
        /// </summary>
        /// <param name="angleROI"></param>
        /// <returns></returns>
        public static List<PathFigure> CreateMiddleArcCrossPath(PointerAngleCoordinate angleROI)
        {
            double delta = ApplyPercentage(angleROI.OuterRadius, _linePercentage);

            if (delta > _maxLineLength)
            {
                delta = _maxLineLength;
            }

            Point arcMiddlePoint = angleROI.GetArcMiddlePoint();

            return new List<PathFigure>
            {
                //hor
                CreateCenterLineOnPoint(arcMiddlePoint, delta, 0).ToPathFigure(),

                //ver
                CreateCenterLineOnPoint(arcMiddlePoint, 0, delta).ToPathFigure(),
            };
        }

        #region Private Methods
        /// <summary>
        ///     Applies a relational percentage to a given value.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="percentage"></param>
        /// <returns></returns>
        private static double ApplyPercentage(double value, double percentage)
        {
            return value * (percentage / 100.0);
        }

        /// <summary>
        ///     Calculates a scalable centered line based on a height and width.
        /// </summary>
        /// <param name="boxWidth"></param>
        /// <param name="boxHeight"></param>
        private static Line CreateCenterLineOnBox(double boxWidth, double boxHeight, double deltaX, double deltaY)
        {
            Point start = new Point((boxWidth / 2) - deltaX, (boxHeight / 2) - deltaY);
            Point end   = new Point(boxWidth - start.X, boxHeight - start.Y);

            return new Line(start, end);
        }

        /// <summary>
        ///     Calculates a new line based on middle point.
        /// </summary>
        /// <param name="middlePoint"></param>
        /// <param name="deltaX"></param>
        /// <param name="deltaY"></param>
        /// <returns></returns>
        private static Line CreateCenterLineOnPoint(Point middlePoint, double deltaX, double deltaY)
        {
            Point start = new Point(middlePoint.X - deltaX , middlePoint.Y - deltaY );
            Point end   = new Point(middlePoint.X + deltaX,  middlePoint.Y + deltaY);

            return new Line(start, end);
        }
        #endregion
    }

    public static class PathExtensions
    {
        /// <summary>
        ///     Converts a line into a path figure.
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public static PathFigure ToPathFigure(this Line line, bool isClosed= false)
        {
            return new PathFigure(line.GetStartPoint(), new List<PathSegment> { new LineSegment(line.GetDestPoint(), true) }, false);
        }
    }
}
