﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace MVVMShapes.Utilities
{
    public static class PolygonUtilities
    {
        #region Polygon Internal Methods

        //source: https://www.geeksforgeeks.org/how-to-check-if-a-given-point-lies-inside-a-polygon/
        
        // Given three colinear Points p, q, r,  
        // the function checks if Point q lies 
        // on line segment 'pr' 
        private static bool OnSegment(Point p, Point q, Point r)
        {
            if (q.X <= Math.Max(p.X, r.X) &&
                q.X >= Math.Min(p.X, r.X) &&
                q.Y <= Math.Max(p.Y, r.Y) &&
                q.Y >= Math.Min(p.Y, r.Y))
            {
                return true;
            }
            return false;
        }

        // To find orientation of ordered triplet (p, q, r). 
        // The function returns following values 
        // 0 --> p, q and r are colinear 
        // 1 --> Clockwise 
        // 2 --> Counterclockwise 
        private static int Orientation(Point p, Point q, Point r)
        {
            double val = (q.Y - p.Y) * (r.X - q.X) - (q.X - p.X) * (r.Y - q.Y);

            if (val == 0)
            {
                return 0; // colinear 
            }

            return (val > 0) ? 1 : 2;
        }

        // The function that returns true if  
        // line segment 'p1q1' and 'p2q2' intersect. 
        private static bool DoIntersect(Point p1, Point q1, Point p2, Point q2)
        {
            // Find the four orientations needed for  
            // general and special cases 
            int o1 = Orientation(p1, q1, p2);
            int o2 = Orientation(p1, q1, q2);
            int o3 = Orientation(p2, q2, p1);
            int o4 = Orientation(p2, q2, q1);

            // General case 
            if (o1 != o2 && o3 != o4)
            {
                return true;
            }

            // Special Cases 
            // p1, q1 and p2 are colinear and 
            // p2 lies on segment p1q1 
            if (o1 == 0 && OnSegment(p1, p2, q1))
            {
                return true;
            }

            // p1, q1 and p2 are colinear and 
            // q2 lies on segment p1q1 
            if (o2 == 0 && OnSegment(p1, q2, q1))
            {
                return true;
            }

            // p2, q2 and p1 are colinear and 
            // p1 lies on segment p2q2 
            if (o3 == 0 && OnSegment(p2, p1, q2))
            {
                return true;
            }

            // p2, q2 and q1 are colinear and 
            // q1 lies on segment p2q2 
            if (o4 == 0 && OnSegment(p2, q1, q2))
            {
                return true;
            }

            // Doesn't fall in any of the above cases 
            return false;
        }
        #endregion

        /// <summary>
        ///     Determines if the given point is inside a set of points, considering edges.
        ///     Only works for a set of points length greater or equal than 3.
        /// </summary>
        /// <param name="polygon"></param>
        /// <param name="testPoint"></param>
        public static bool IsPointInPolygon(List<Point> polygon, Point testPoint)
        {
            int length = polygon.Count;

            if (length < 3)
            {
                return false;
            }

            // Create a Point for line segment from p to infinite 
            Point extreme = new Point(double.MaxValue, testPoint.Y);

            // Count intersections of the above line  
            // with sides of polygon 
            int count = 0, i = 0;
            do
            {
                int next = (i + 1) % length;

                // Check if the line segment from 'p' to  
                // 'extreme' intersects with the line  
                // segment from 'polygon[i]' to 'polygon[next]' 
                if (DoIntersect(polygon[i], polygon[next], testPoint, extreme))
                {
                    // If the Point 'p' is colinear with line  
                    // segment 'i-next', then check if it lies  
                    // on segment. If it lies, return true, otherwise false 
                    if (Orientation(polygon[i], testPoint, polygon[next]) == 0)
                    {
                        return OnSegment(polygon[i], testPoint, polygon[next]);
                    }

                    count++;
                }

                i = next;
            } 
            while (i != 0);

            return (count % 2 == 1);
        }
    }
}
