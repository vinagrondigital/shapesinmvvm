﻿using System.Windows.Input;

namespace MVVMShapes.Utilities
{
    public static class CursorUtilities
    {
        private const int _rangeTolerance = 20;

        public static Cursor GetCursorFromAngle(double angle)
        {
            //format angle
            angle = GeometryUtilities.GetFormattedAngleTo360(angle);

            switch(true)
            {
                case true when  GeometryUtilities.IsArcInRange(angle, 360 - _rangeTolerance, 0 + _rangeTolerance) || 
                                GeometryUtilities.IsArcInRange(angle, 180 - _rangeTolerance, 180 + _rangeTolerance) :
                    return Cursors.SizeNS;
                case true when GeometryUtilities.IsArcInRange(angle, 90 - _rangeTolerance, 90 + _rangeTolerance) ||
                               GeometryUtilities.IsArcInRange(angle, 270 - _rangeTolerance, 270 + _rangeTolerance):
                    return Cursors.SizeWE;

                case true when GeometryUtilities.IsArcInRange(angle, 0 - _rangeTolerance, 90 + _rangeTolerance) ||
                               GeometryUtilities.IsArcInRange(angle, 180 - _rangeTolerance, 270 + _rangeTolerance):
                    return Cursors.SizeNWSE;
                case true when GeometryUtilities.IsArcInRange(angle, 90 - _rangeTolerance, 180 + _rangeTolerance) ||
                               GeometryUtilities.IsArcInRange(angle, 270 - _rangeTolerance, 360 + _rangeTolerance):
                    return Cursors.SizeNESW;
                default:
                    return Cursors.Arrow;
            }
        }

        public static Cursor GetCursorFromRadius(double angle)
        {
            //format angle
            angle = GeometryUtilities.GetFormattedAngleTo360(angle);

            switch (true)
            {
                case true when GeometryUtilities.IsArcInRange(angle, 360 - _rangeTolerance, 0 + _rangeTolerance) ||
                                GeometryUtilities.IsArcInRange(angle, 180 - _rangeTolerance, 180 + _rangeTolerance):
                    return Cursors.SizeWE;
                case true when GeometryUtilities.IsArcInRange(angle, 90 - _rangeTolerance, 90 + _rangeTolerance) ||
                               GeometryUtilities.IsArcInRange(angle, 270 - _rangeTolerance, 270 + _rangeTolerance):
                    return Cursors.SizeNS;

                case true when GeometryUtilities.IsArcInRange(angle, 0 - _rangeTolerance, 90 + _rangeTolerance) ||
                               GeometryUtilities.IsArcInRange(angle, 180 - _rangeTolerance, 270 + _rangeTolerance):
                    return Cursors.SizeNESW;
                case true when GeometryUtilities.IsArcInRange(angle, 90 - _rangeTolerance, 180 + _rangeTolerance) ||
                               GeometryUtilities.IsArcInRange(angle, 270 - _rangeTolerance, 360 + _rangeTolerance):
                    return Cursors.SizeNWSE;
                default:
                    return Cursors.Arrow;
            }
        }
    }
}
