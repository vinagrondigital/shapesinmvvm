﻿using System;
using System.Windows;

namespace MVVMShapes.Utilities
{
    public static class GeometryUtilities
    {
        /// <summary>
        ///     Gets a point for the defined circle with a center, radius and angle.
        /// </summary>
        /// <param name="center">Center of the circle</param>
        /// <param name="radius">Radius if the circle</param>
        /// <param name="angle">Angle, units are declared in parameter</param>
        /// <param name="isDegreesAngle">When true, angle is treated on degrees; otherwise, in radians</param>
        /// <returns></returns>
        public static Point GetCirclePoint(Point center, double radius, double angle, bool isDegreesAngle=true)
        {
            double radians = (isDegreesAngle)? ConvertToRadians(angle): angle;

            return new Point()
            {
                X = center.X + radius * Math.Cos(radians),
                Y = center.Y - radius * Math.Sin(radians),
            };
        }

        /// <summary>
        ///     Gets the angle formed by two points.
        /// </summary>
        /// <param name="source">The initial point</param>
        /// <param name="destination">The final point</param>
        /// <param name="resultInDegrees">When true, the result will be formatted on degrees</param>
        /// <param name="forcePositive">When true, the angle is forced to be in the format [0, 360] degrees or [0, 2Pi] rad</param>
        /// <returns></returns>
        public static double GetAngle(Point source, Point destination, bool resultInDegrees = true, bool forcePositive=true)
        {
            return GetAngle(destination.Y - source.Y, destination.X - source.X, forcePositive, resultInDegrees);
        }

        /// <summary>
        ///     Gets the angle formed by two points considering that Y axis is inverted (Y axis goes positive from top to bottom).        
        /// </summary>
        /// <param name="source">The initial point</param>
        /// <param name="destination">The final point</param>
        /// <param name="resultInDegrees">When true, the result will be formatted on degrees</param>
        /// <param name="forcePositive">When true, the angle is forced to be in the format [0, 360] degrees or [0, 2Pi] rad</param>
        /// <returns></returns>
        public static double GetAngleOnYaxisInverted(Point source, Point destination, bool resultInDegrees = true, bool forcePositive = true)
        {
            //on Y axis inverted, ydiff difference should be sign-inverted
            return GetAngle(-(destination.Y - source.Y), destination.X - source.X, forcePositive, resultInDegrees);
        }

        #region Angle Internal Methods
        private static double GetAngle(double ydiff, double xdiff, bool resultInDegrees = true, bool forcePositive = true)
        {
            //get angle in radians
            double angleInRadians = (forcePositive) ? GetPositiveAngle(xdiff, ydiff) : Math.Atan2(ydiff, xdiff);

            //convert to degrees when needed it
            return (resultInDegrees) ? ConvertToDegrees(angleInRadians) : angleInRadians;
        }

        private static double GetPositiveAngle(double xdiff, double ydiff)
        {
            // ArcTan Math function returns angle in radians and in the form -Pi <= 0 <= Pi
            // A short way to make it positive and in the form of 0 <= theta <= 2*Pi
            // is to make a no-sign computing and the compensate it depending on the quadrant
            double angleInRadians = Math.Atan2(Math.Abs(ydiff), Math.Abs(xdiff));

            switch (true)
            {
                case true when ydiff > 0 && xdiff > 0:
                    break;
                case true when ydiff > 0 && xdiff < 0:
                    angleInRadians = (Math.PI / 2 - angleInRadians) + Math.PI / 2;
                    break;
                case true when ydiff < 0 && xdiff < 0:
                    angleInRadians += Math.PI;
                    break;
                case true when ydiff < 0 && xdiff > 0:
                    angleInRadians = (Math.PI / 2 - angleInRadians) + 3 * Math.PI / 2;
                    break;
                default:
                    break;
            }

            return angleInRadians;
        }

        #endregion

        /// <summary>
        ///     Checks if a value is within range of a certain minimum and maximum value.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static bool IsInRange(double value, double min, double max)
        {
            return (max >= value && value >= min);
        }

        /// <summary>
        ///     Checks if an angle is within range of a certain angle start and angle end.
        /// </summary>
        /// <param name="angleInDegrees"></param>
        /// <param name="angleStart"></param>
        /// <param name="angleEnd"></param>
        /// <returns></returns>
        public static bool IsArcInRange(double angleInDegrees, double angleStart, double angleEnd)
        {
            angleInDegrees = GetFormattedAngleTo360(angleInDegrees);
            angleStart     = GetFormattedAngleTo360(angleStart);
            angleEnd       = GetFormattedAngleTo360(angleEnd);

            while (angleEnd < angleStart)
            {
                angleEnd += 360;
            }

            while (angleInDegrees < angleStart)
            {
                angleInDegrees += 360;
            }

            return IsInRange(angleInDegrees, angleStart, angleEnd);
        }

        /// <summary>
        ///     Forces an angle to be formatted in the range of [0, 360].
        /// </summary>
        /// <param name="angleInDegrees"></param>
        /// <returns></returns>
        public static double GetFormattedAngleTo360(double angleInDegrees)
        {
            while (angleInDegrees > 360)
            {
                angleInDegrees -= 360;
            }

            while (angleInDegrees < 0)
            {
                angleInDegrees += 360;
            }

            return angleInDegrees;
        }

        /// <summary>
        ///     Forces an angle to be formatted in the range of [-180, -180].
        /// </summary>
        /// <param name="angleInDegrees"></param>
        /// <returns></returns>
        public static double GetFormattedAngleTo180(double angleInDegrees)
        {
            while (angleInDegrees > 180)
            {
                angleInDegrees -= 360;
            }

            while (angleInDegrees < -180)
            {
                angleInDegrees += 360;
            }

            return angleInDegrees;
        }

        /// <summary>
        ///     Converts an angle from degrees to radians.
        /// </summary>
        /// <param name="angleInDegrees"></param>
        /// <returns></returns>
        public static double ConvertToRadians(double angleInDegrees)
        {
            return angleInDegrees * Math.PI / 180.0;
        }

        /// <summary>
        ///     Converts an angle from radians to degrees.
        /// </summary>
        /// <param name="angleInRadians"></param>
        /// <returns></returns>
        public static double ConvertToDegrees(double angleInRadians)
        {
            return angleInRadians * 180.0 / Math.PI;
        }

        public static bool IsBigArc(double thetaStart, double thetaEnd)
        {
            //matematicas hijo
            if (thetaStart > thetaEnd)
            {
                return (360 - (thetaStart - thetaEnd)) > 180;
            }
            else
            {
                return (thetaEnd - thetaStart) > 180;
            }
        }

        public static double GetDistanceBetweenPoints(Point p1, Point p2)
        {
            return Math.Sqrt(Math.Pow(p2.X - p1.X, 2) + Math.Pow(p2.Y - p1.Y, 2));
        }
    }
}
