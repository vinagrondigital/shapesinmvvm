﻿using MVVMShapes.Interfaces;
using MVVMShapes.Models;
using MVVMShapes.Utilities;
using System;
using System.Collections.Generic;
using System.Windows.Media;

namespace MVVMShapes.SearchZones
{
    /// <summary>
    /// Represents a rectangular search zone
    /// </summary>
    public class RectangularZone : Zone
    {
        public RectangularZone(int id, double left, double top, DesignerItemType type = DesignerItemType.RectangularROI, IImageEditor parent = null)
            : base(id, left, top, type, parent)
        {

        }

        public RectangularZone(List<DBCoordinate> coords, IImageEditor parent)
            : base(DesignerItemType.RectangularROI, parent)
        {
            //sanitize this
            if (coords == null)
            {
                throw new NullReferenceException(nameof(coords));
            }

            if (coords.Count < 2)
            {
                throw new InvalidOperationException($"RectangularZone needs at least 2 coordinates. given: {Coordinates.Count}");
            }

            //create the rectangle using the given DB coordinates
            double diffX = Math.Abs(coords[0].X - coords[1].X);
            double diffY = Math.Abs(coords[0].Y - coords[1].Y);

            Left = coords[0].X;
            Top  = coords[0].Y;
            ItemWidth  = Math.Round(diffX, 2);
            ItemHeight = Math.Round(diffY, 2);

            Constraint = DesignerItemConstraint.None;

            //create the zone
            DrawShape();
        }
    }
}
