﻿using MVVMShapes.Interfaces;
using MVVMShapes.Models;
using MVVMShapes.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Media;

namespace MVVMShapes.SearchZones
{
    public enum Radius
    {
        Inner,
        Outer,
    }

    public class ConverterContext
    {
        public List<Point> MyPoints { get; set; }
        public PointerAngleCoordinate CenterCoord { get; set; }
    }

    /// <summary>
    /// Represents a pointerAngle zone
    /// </summary>
    public class PointerAngleZone : Zone
    {
        #region Private data
        public static readonly int _boxMargin = 3;
        #endregion

        #region Public data
        public PointerAngleCoordinate CenterCoord { get; set; }
        //public ConvTest MyTest { get; set; }

        private ConverterContext _myTest;
        public ConverterContext MyTest
        {
            get { return _myTest; }
            set { SetProperty(ref _myTest, value); }
        }
        #endregion

        public PointerAngleZone(DBCoordinate dbOriginCoord, IImageEditor parent)
            : base(DesignerItemType.PointerAngleROI, parent)
        {
            //sanitize this
            if (dbOriginCoord == null)
            {
                throw new ArgumentNullException(nameof(dbOriginCoord));
            }

            //set min dimensions
            SetMinWidthHeight(20, 20);

            //get a center coordinate for the pointer angle roi
            CenterCoord = new PointerAngleCoordinate(dbOriginCoord);

            //re-draw shape 
            DrawShape();
        }

        public bool ExtendRadius(double delta, Radius whichRadius)
        {
            if(delta == 0)
            {
                return false;
            }

            if(whichRadius == Radius.Inner)
            {
                return ExtendInnerRadius(delta);
            }
            else if(whichRadius == Radius.Outer)
            {
                return ExtendOuterRadius(delta);
            }
            else
            {
                return false;
            }
        }

        public bool SetAngle(double angle, Theta selectedAngle)
        {
            double boundaryY = Math.Sin(GeometryUtilities.ConvertToRadians(angle)) * CenterCoord.OuterRadius;

            //Console.WriteLine($"BY = {boundaryY} Ymax = {MyPoints.Select(p => p.Y).Max()} height={ItemHeight} cc={CenterCoord.Center.Y - boundaryY}");
            Console.WriteLine($"Top{Top} height{ItemHeight} boundarY{boundaryY} calc{Top + ItemHeight - boundaryY}");

            //assign angle
            CenterCoord.SetAngle(angle, selectedAngle);

            //readjust to ensure a minimum arc
            CenterCoord.ReadjustWithMinimum(5, selectedAngle);

            return true;
        }


        public override void DrawShape()
        {
            CenterCoord.Center = new Point(CenterCoord.Center.X + Left, CenterCoord.Center.Y + Top);

            //calculate the relative left & top for the canvas rectangle that holds the angle zone roi
            BoundingBox limitbox = BoundingBox.CreateNew(CenterCoord, includeCenter:true);

            //add a little margin to the object
            limitbox.AddMargin(_boxMargin);

            //set top and height and item width/height            
            Top        = limitbox.Top;
            Left       = limitbox.Left;
            ItemWidth  = limitbox.Width;
            ItemHeight = limitbox.Height;

            //get a new center
            CenterCoord.Center = new Point(CenterCoord.Center.X - Left, CenterCoord.Center.Y - Top);

            //if negative top/left is presented, boundary is re-set
            Top = Math.Max(0, Top);
            Left = Math.Max(0, Left);

            //create a new points list with the modified (offset'd) roi center coordinate
            MyPoints = new ObservableCollection<Point>(CenterCoord.GetCoordinateList());

            MyTest = new ConverterContext()
            {
                CenterCoord = CenterCoord,
                MyPoints = MyPoints.ToList(),
            };

            base.DrawShape();
        }

        /// <summary>
        /// Calculate the object geometry from the center coordinate
        /// </summary>
        /// <param name="CenterCoord">center coordinate</param>
        /// <returns>PathGeometry with the shape</returns>
        private bool ExtendOuterRadius(double newRadius)
        {
            //check bounds with inner radius
            if (newRadius <= (CenterCoord.InnerRadius + 10))    //TODO change with object property
            {
                return false;
            }

            double rdiff = (newRadius - CenterCoord.OuterRadius);

            //check bounds with parent canvas X & Y
            if (Parent.CheckXBoundary(Left + ItemWidth + rdiff) == false 
                || Parent.CheckYBoundary(Top + ItemHeight + rdiff) == false)
            {
                return false;
            }

            //modify the outer radius
            CenterCoord.OuterRadius = newRadius;

            return true;
        }

        private bool ExtendInnerRadius(double newRadius)
        {
            //check bounds with inner radius
            if (newRadius >= (CenterCoord.OuterRadius - 10))    //TODO change with object property
            {
                return false;
            }

            if (newRadius < 10)   //todo change this
            {
                return false;
            }

            //modify the outer radius
            CenterCoord.InnerRadius = newRadius;

            return true;
        }
    }
}