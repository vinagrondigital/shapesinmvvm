﻿using System.Windows.Media;

namespace MVVMShapes.Interfaces
{
    public interface IZone
    {
        GeometryGroup ZoneGeometry { get; set; }
        void DrawShape();
    }
}
