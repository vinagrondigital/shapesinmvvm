﻿using MVVMShapes.Models;
using System.Windows.Media;

namespace MVVMShapes.Interfaces
{
    /// <summary>
    /// Describes a RegionOfInterest (ROI)
    /// </summary>
    public interface IEditorRoi
    {
        /// <summary>
        /// Canvas left coordinate (X)
        /// </summary>
        double Left { get; set; }

        /// <summary>
        /// Canvas top coordinate (Y)
        /// </summary>
        double Top { get; set; }

        /// <summary>
        /// Width
        /// </summary>
        double ItemWidth { get; set; }

        /// <summary>
        /// Minimum width
        /// </summary>
        double MinWidth { get; set; }

        /// <summary>
        /// Height
        /// </summary>
        double ItemHeight { get; set; }

        /// <summary>
        /// Minimum height
        /// </summary>
        double MinHeight { get; set; }

        /// <summary>
        /// RoiType
        /// </summary>
        DesignerItemType RoiType { get; set; }

        /// <summary>
        /// Must implement the required logic for item selection
        /// </summary>
        void Select();

        /// <summary>
        /// Must implement the required logic for item de-selection
        /// </summary>
        void Deselect();
    }
}
