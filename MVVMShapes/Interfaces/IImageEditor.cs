﻿using MVVMShapes.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;

namespace MVVMShapes.Interfaces
{    
    /// <summary>
    /// Implements the required stuff for an ImageEditor object
    /// </summary>
    public interface IImageEditor
    {
        /// <summary>
        /// DesignerItemModelBase Collection
        /// </summary>
        ObservableCollection<DesignerItemModelBase> Items { get; set; }

        /// <summary>
        /// Canvas size
        /// </summary>
        Point CanvasSize { get; set; }

        /// <summary>
        /// Set the working XY coordinates
        /// </summary>
        /// <param name="point">Mouse/misc coordinate</param>
        void SetXYCoordinate(Point point);

        Point GetXYCoordinate();

        /// <summary>
        /// Deletes all items from the editor list
        /// </summary>
        /// <param name="onlyDeleteSelected">if false, all the items will be deleted</param>
        void DeleteAllItems(bool onlyDeleteSelected);

        /// <summary>
        /// Creates an item from the specified coordinate list & the item type
        /// </summary>
        /// <param name="coords">coordinate list</param>
        /// <param name="type">item type</param>
        void CreateItem(List<DBCoordinate> coords, DesignerItemType type);

        /// <summary>
        /// Creates an item from a single coordinate object
        /// </summary>
        /// <param name="coord">coordinate</param>
        /// <param name="type">item type</param>
        void CreateItem(DBCoordinate coord, DesignerItemType type);

        /// <summary>
        /// Check the boundary for the given DesignerItemModelBase object
        /// </summary>
        /// <param name="obj"></param>
        void CheckBounds(DesignerItemModelBase obj);

        /// <summary>
        /// Checks the boundary for the given X coordinate, returns true if passed height is within the canvas
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        bool CheckXBoundary(double width);

        /// <summary>
        /// Checks the boundary for the given Y coordinate, returns true if passed height is within the canvas
        /// </summary>
        /// <param name="height"></param>
        /// <returns></returns>
        bool CheckYBoundary(double height);

        /// <summary>
        /// Checks the boundary for the given XY coordinate, returns true if passed coords are within the canvas
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        bool CheckXYBoundaries(double width, double height);

        /// <summary>
        /// Deselects all items        
        /// </summary>
        void DeselectAllItems();

        /// <summary>
        /// Deselects all items        
        /// </summary>
        void SelectAllItems();
    }
}
