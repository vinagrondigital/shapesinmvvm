﻿using MVVMShapes.Models;

namespace MVVMShapes.Interfaces
{
    /// <summary>
    /// Imple
    /// </summary>
    public interface IDesignItem
    {
        bool IsSelected { get; set; }
        IImageEditor Parent { get; set; }
        DesignerItemConstraint Constraint { get; set; }
    }
}
