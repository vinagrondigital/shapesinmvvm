﻿using MVVMShapes.Editors;

namespace MVVMShapes.Interfaces
{
    /// <summary>
    /// Implements the required stuff for an EditorDocument
    /// </summary>
    public interface IEditorDocument
    {
        ImageEditor ImgEditor { get; set; }
    }
}
