﻿using MVVMShapes.Interfaces;
using MVVMShapes.Models;
using MVVMShapes.SearchZones;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;

namespace MVVMShapes.Editors
{
    /// <summary>
    /// iDEA image editor object
    /// Handles an item list and provides methods & properties for easy (MVVM Compatible) control
    /// </summary>
    public class ImageEditor : BindableBase, IImageEditor
    {
        #region Bindable stuff
        private ObservableCollection<DesignerItemModelBase> _items;
        public ObservableCollection<DesignerItemModelBase> Items
        {
            get { return _items; }
            set { SetProperty(ref _items, value); }
        }

        private Point _xyCoord;
        public Point XYCoord
        {
            get { return _xyCoord; }
            set { SetProperty(ref _xyCoord, value); }
        }

        private Point _canvasSize;
        public Point CanvasSize
        {
            get { return _canvasSize; }
            set { SetProperty(ref _canvasSize, value); }
        }
        #endregion

        /// <summary>
        /// Parameterless ctor, initializes the item list
        /// </summary>
        public ImageEditor(double width = 720, double height = 480)
        {
            Items = new ObservableCollection<DesignerItemModelBase>();

            //set the canvas size
            SetCanvasSize(width, height);
        }


        /// <summary>
        /// Initializes the editor, creates and adds a bitmap item to the list
        /// </summary>
        /// <param name="bitmapPath">bitmap path on disk</param>
        public ImageEditor(string bitmapPath) : this()
        {
            //add the image
            Items.Add(new EditorBitmapItem(bitmapPath));

            SetCanvasSize(Items[0].ItemWidth, Items[0].ItemHeight);
        }

        public void SetCanvasSize(double width, double height, int roundTo = 0)
        {
            CanvasSize = new Point()
            {
                X = Math.Round(width, roundTo),
                Y = Math.Round(height, roundTo)
            };
        }

        /// <summary>
        /// Creates an item from the specified coordinate list & the item type
        /// </summary>
        /// <param name="coords">coordinate list</param>
        /// <param name="type">item type</param>
        public void CreateItem(List<DBCoordinate> coords, DesignerItemType type)
        {
            //TODO: sanitize this
            //use factoy pattern later?
            switch (type)
            {
                case DesignerItemType.RectangularROI:
                    Items.Add(new RectangularZone(coords, this));
                    break;

                case DesignerItemType.PointerAngleROI:
                    Items.Add(new PointerAngleZone(coords[0], this));
                    break;

                case DesignerItemType.Bitmap:
                    throw new InvalidOperationException($"Unsupported");

                default:
                    throw new InvalidOperationException($"ItemType: '{type}' is invalid");
            }
        }

        /// <summary>
        /// Creates an item from a single coordinate object
        /// </summary>
        /// <param name="coord">coordinate</param>
        /// <param name="type">item type</param>
        public void CreateItem(DBCoordinate coord, DesignerItemType type)
        {
            switch (type)
            {
                case DesignerItemType.RectangularROI:
                    throw new InvalidOperationException("Rectangular ROI needs two DB coordinates");

                case DesignerItemType.PointerAngleROI:
                    Items.Add(new PointerAngleZone(coord, this));
                    break;

                case DesignerItemType.Bitmap:
                    throw new InvalidOperationException($"Unsupported");

                default:
                    throw new InvalidOperationException($"ItemType: '{type}' is invalid");
            }
        }

        /// <summary>
        /// Deletes all items from the editor list
        /// </summary>
        /// <param name="onlyDeleteSelected">if false, all the items will be deleted</param>
        public void DeleteAllItems(bool onlyDeleteSelected = false)
        {
            List<DesignerItemModelBase> list = Items.ToList();

            foreach (DesignerItemModelBase item in list)
            {
                if (onlyDeleteSelected && item.IsSelected == false || item.RoiType == DesignerItemType.Bitmap)
                {
                    //skip this item
                    continue;
                }

                Items.Remove(item);
            }
        }

        /// <summary>
        /// Deselects all items        
        /// </summary>
        public void DeselectAllItems()
        {
            SelectAll(false);
        }

        /// <summary>
        /// Selects all items
        /// </summary>
        public void SelectAllItems()
        {
            SelectAll(true);
        }

        /// <summary>
        /// Sets the working XY coordinate
        /// </summary>
        /// <param name="point">Mouse/misc coordinate</param>
        public void SetXYCoordinate(Point point)
        {
            if (point == null)
            {
                throw new ArgumentNullException(nameof(point));
            }

            XYCoord = new Point()
            {
                X = Math.Round(point.X, 3),
                Y = Math.Round(point.Y, 3),
            };
        }

        /// <summary>
        /// Private selection implementation, iterates all items in the list and sets the selection
        /// </summary>
        /// <param name="IsSelected">isSelected</param>
        private void SelectAll(bool IsSelected)
        {
            foreach (DesignerItemModelBase item in Items)
            {
                if (item.RoiType != DesignerItemType.Bitmap)
                {
                    item.IsSelected = IsSelected;
                }
            }
        }

        public Point GetXYCoordinate()
        {
            return XYCoord;
        }

        /// <summary>
        /// Child items must check this function to avoid out of bounds resizing & other weird visual stuff
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public void CheckBounds(DesignerItemModelBase obj)
        {
            if(obj.Left + obj.ItemWidth > CanvasSize.X)
            {
                //obj.ri
                return;
            }
        }

        /// <summary>
        /// Checks the boundary for the given X coordinate, returns true if passed height is within the canvas
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public bool CheckXBoundary(double width)
        {
            return width < CanvasSize.X;
        }

        /// <summary>
        /// Checks the boundary for the given Y coordinate, returns true if passed height is within the canvas
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public bool CheckYBoundary(double height)
        {
            return height > 0 && height < CanvasSize.Y;
        }

        /// <summary>
        /// Checks the boundary for the given XY coordinate, returns true if passed coords are within the canvas
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        public bool CheckXYBoundaries(double width, double height)
        {
            return (width < CanvasSize.X) && (width < CanvasSize.Y);
        }
    }
}
