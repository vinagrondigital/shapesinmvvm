﻿using MVVMShapes.Interfaces;
using MVVMShapes.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace MVVMShapes.Editors
{
    public class EditorBitmapItem : DesignerItemModelBase
    {

        private BitmapImage _imgSource;
        public BitmapImage ImgSource
        {
            get { return _imgSource; }
            set { SetProperty(ref _imgSource, value); }
        }

        public EditorBitmapItem(int id, double left, double top, DesignerItemType type = DesignerItemType.Bitmap, IImageEditor parent = null)
            : base(id, left, top, type, parent)
        {
            ItemHeight = 100;
            ItemWidth = 100;
        } 

        public EditorBitmapItem(double width, double height) : base()
        {
            ItemWidth = width;
            ItemHeight = height;

            Left = 0;
            Top = 0;

            Parent = null;
            RoiType = DesignerItemType.Bitmap;
        }

        public EditorBitmapItem(string path) : base()
        {
            Left = Top = 0;
            Parent = null;
            RoiType = DesignerItemType.Bitmap;

            try
            {
                ImgSource = LoadImageFromPath(path);
            }
            catch (IOException ex)
            {
                //LOG THIS TODO
            }

            ItemWidth = ImgSource.Width;
            ItemHeight = ImgSource.Height;            
        }

        private static BitmapImage LoadImageFromPath(string filepath)
        {
            Uri pathUri = new Uri(filepath);

            if (File.Exists(filepath) == false)
            {
                //try to load the default "Img not available"
                return new BitmapImage(pathUri);
            }
            else
            {
                return new BitmapImage(pathUri);
            }
        }

    }
}
