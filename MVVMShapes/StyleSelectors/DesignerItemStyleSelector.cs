﻿using MVVMShapes.Interfaces;
using MVVMShapes.Models;
using System;
using System.Windows;
using System.Windows.Controls;

namespace MVVMShapes.StyleSelectors
{
    public class DesignerItemStyleSelector : StyleSelector
    {
        static DesignerItemStyleSelector()
        {
            Instance = new DesignerItemStyleSelector();
        }

        public static DesignerItemStyleSelector Instance
        {
            get;
            private set;
        }

        //TODO: check testExec on style selectors
        public override Style SelectStyle(object item, DependencyObject container)
        {
            ItemsControl itemsControl = ItemsControl.ItemsControlFromItemContainer(container);

            //sanitize this
            if (itemsControl == null)
            {
                throw new InvalidOperationException("DesignerItemStyleSelector : Could not create items control");
            }

            //check if we have the right object type
            if (item is DesignerItemModelBase designerItem)
            {                                

                if(designerItem.RoiType == DesignerItemType.Bitmap)
                {
                    //not sure about this...
                    return (Style)itemsControl.FindResource("designerItemStyle2");
                }
                else if(designerItem.RoiType == DesignerItemType.PointerAngleROI)
                {
                    return (Style)itemsControl.FindResource("PointerAngleRoiStyle");                    
                }
                else
                {
                    //not sure about this...
                    return (Style)itemsControl.FindResource("designerItemStyle");
                }
            }

            //nothing found
            return null;
        }
    }
}
