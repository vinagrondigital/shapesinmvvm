﻿using IdeaToolkit.Zoombox;
using System.Windows;
using System.Windows.Interactivity;

namespace MVVMShapes.Behaviors
{
    /// <summary>
    /// Manages content auto fit to bounds at load for iDEAZoombox control
    /// </summary>
    public class ZoomBoxBehavior : Behavior<Zoombox>
    {
        protected override void OnAttached()
        {
            base.OnAttached();

            AssociatedObject.Loaded += new RoutedEventHandler(OnLoaded);
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            AssociatedObject.Loaded -= OnLoaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            if (sender is Zoombox zb)
            {
                zb.FitToBounds();
            }
        }
    }
}
