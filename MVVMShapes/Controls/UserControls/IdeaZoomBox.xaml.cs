﻿using MVVMShapes.Editors;
using System.Windows;
using System.Windows.Controls;

namespace MVVMShapes.Controls
{
    /// <summary>
    /// Interaction logic for IdeaZoomBox.xaml
    /// </summary>
    public partial class IdeaZoomBox : UserControl
    {
        public static readonly DependencyProperty ImageEditorSourceProperty = DependencyProperty.Register("ImageEditorSource", typeof(ImageEditor), typeof(IdeaZoomBox));

        public ImageEditor ImageEditorSource
        {
            get { return (ImageEditor)GetValue(ImageEditorSourceProperty); }
            set { SetValue(ImageEditorSourceProperty, value); }
        }

        public static readonly DependencyProperty MaxScaleProperty = DependencyProperty.Register("MaxScaleSource", typeof(double), typeof(IdeaZoomBox), new PropertyMetadata(10.0));
        public double MaxScaleSource
        {
            get { return (double)GetValue(MaxScaleProperty); }
            set { SetValue(MaxScaleProperty, value); }
        }

        public static readonly DependencyProperty MinScaleProperty = DependencyProperty.Register("MinScaleSource", typeof(double), typeof(IdeaZoomBox), new PropertyMetadata(0.1));
        public double MinScaleSource
        {
            get { return (double)GetValue(MinScaleProperty); }
            set { SetValue(MinScaleProperty, value); }
        }

        public IdeaZoomBox()
        {
            InitializeComponent();
            //ZBox.AnimationCompleted += ZBox_AnimationCompleted;
        }

        private void CenterClick(object sender, RoutedEventArgs e)
        {
            ZBox.CenterContent();
        }

        private void FillClick(object sender, RoutedEventArgs e)
        {
            ZBox.FillToBounds();
        }

        private void FitClick(object sender, RoutedEventArgs e)
        {
            ZBox.FitToBounds();
        }

        private void ZoomToOneHundred(object sender, RoutedEventArgs e)
        {
            ZBox.ZoomTo(1.0);
        }
    }
}
