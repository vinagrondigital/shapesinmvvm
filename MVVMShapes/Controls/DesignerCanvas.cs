﻿using MVVMShapes.Adorners;
using MVVMShapes.Interfaces;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;

namespace MVVMShapes.Controls
{
    /// <summary>
    /// iDEA DesignerCanvas for drawing objects on screen, MVVM compatible
    /// </summary>
    public class DesignerCanvas : Canvas
    {
        private Point? _selectionStartPoint;

        public DesignerCanvas()
        {
            _selectionStartPoint = null;
        }

        #region Mouse events override
        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);

            if (e.LeftButton == MouseButtonState.Pressed)
            {
                //if we are source of event, we are rubberband selecting
                if (e.Source == this)
                {
                    // in case that this click is the start for a 
                    // drag operation we cache the start point
                    _selectionStartPoint = e.GetPosition(this);

                    //clear all selections in our data context
                    if (DataContext is IEditorDocument vm)
                    {
                        //check that no CTRL key is pressed, when user pressed control it usually means he is selecting more than 1 item
                        if (!(Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl)))
                        {
                            if (vm.ImgEditor != null)
                            {
                                vm.ImgEditor.DeselectAllItems();
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// MouseMove event hook
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            //set the Mouse XY coord to the imgEditor object
            if (DataContext is IEditorDocument vm)
            {
                vm.ImgEditor.SetXYCoordinate(Mouse.GetPosition(this));
            }

            // if mouse button is not pressed we have no drag operation
            if (e.LeftButton != MouseButtonState.Pressed)
            {
                _selectionStartPoint = null;
            }

            // ... but if mouse button is pressed and start point value is set we do have one
            if (_selectionStartPoint.HasValue)
            {
                // create rubberband adorner
                AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(this);
                if (adornerLayer != null)
                {
                    RubberbandAdorner adorner = new RubberbandAdorner(this, _selectionStartPoint);
                    if (adorner != null)
                    {
                        adornerLayer.Add(adorner);
                    }
                }
            }

            e.Handled = true;
        }
        #endregion

        protected override Size MeasureOverride(Size constraint)
        {
            Size size = new Size();

            foreach (UIElement element in InternalChildren)
            {
                double left = GetLeft(element);
                double top = GetTop(element);
                left = double.IsNaN(left) ? 0 : left;
                top = double.IsNaN(top) ? 0 : top;

                //measure desired size for each child
                element.Measure(constraint);

                Size desiredSize = element.DesiredSize;
                if (!double.IsNaN(desiredSize.Width) && !double.IsNaN(desiredSize.Height))
                {
                    size.Width = Math.Max(size.Width, left + desiredSize.Width);
                    size.Height = Math.Max(size.Height, top + desiredSize.Height);
                }
            }

            // add margin 
            /*size.Width += 10;
            size.Height += 10;*/

            return size;
        }
    }
}
