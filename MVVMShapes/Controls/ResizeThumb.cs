﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls.Primitives;

using MVVMShapes.Models;
using MVVMShapes.Utilities;
using MVVMShapes.SearchZones;

namespace MVVMShapes.Controls
{
    /// <summary>
    /// iDEA ResizeThumb. manages the resizing of objects inside an iDEA DesignerCanvas
    /// </summary>
    public class ResizeThumb : Thumb
    {
        private const int _minAngle = 5;
        private double _minWidth = 10, _minHeight = 10;

        public static readonly DependencyProperty ResizeTypeProperty =
           DependencyProperty.RegisterAttached("ResizeType", typeof(ResizeType), typeof(ResizeThumb),
               new FrameworkPropertyMetadata(ResizeType.Default));

        /// <summary>
        /// Get the selectionEnabled value
        /// </summary>
        /// <param name="d">dependency object to get the value from</param>
        /// <returns></returns>
        public static ResizeType GetResizeType(DependencyObject d)
        {
            return (ResizeType)d.GetValue(ResizeTypeProperty);
        }

        /// <summary>
        /// Set the selectionEnabled value
        /// </summary>
        /// <param name="d">dependency object to set the value</param>
        /// <param name="value">selection enabled value</param>
        public static void SetResizeType(DependencyObject d, ResizeType value)
        {
            d.SetValue(ResizeTypeProperty, value);
        }


        public static readonly DependencyProperty ResizeInvertedProperty =
           DependencyProperty.RegisterAttached("ResizeInverted", typeof(bool), typeof(ResizeThumb),
               new FrameworkPropertyMetadata(false));

        /// <summary>
        /// Get the selectionEnabled value
        /// </summary>
        /// <param name="d">dependency object to get the value from</param>
        /// <returns></returns>
        public static bool GetResizeInverted(DependencyObject d)
        {
            return (bool)d.GetValue(ResizeInvertedProperty);
        }

        /// <summary>
        /// Set the selectionEnabled value
        /// </summary>
        /// <param name="d">dependency object to set the value</param>
        /// <param name="value">selection enabled value</param>
        public static void SetResizeInverted(DependencyObject d, bool value)
        {
            d.SetValue(ResizeInvertedProperty, value);
        }

        /// <summary>
        /// Parameterless ctor
        /// </summary>
        public ResizeThumb()
        {
            DragDelta += new DragDeltaEventHandler(OnDragDelta);
        }

        /// <summary>
        /// DragDelta hook
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnDragDelta(object sender, DragDeltaEventArgs e)
        {
            if (DataContext is DesignerItemModelBase item)
            {
                _minWidth  = item.MinWidth;
                _minHeight = item.MinHeight;

                switch (item)
                {
                    case RectangularZone recZone:
                        ApplyRectangleChange(recZone, e);
                        break;

                    case PointerAngleZone angleRoi:
                        ApplyArcChange(angleRoi);
                        break;

                    default:
                        break;
                }
            }
        }

        private void ApplyArcChange(PointerAngleZone angleRoi)
        {
            ResizeType resizeType = GetResizeType(this);
            bool redraw;

            switch (resizeType)
            {
                case ResizeType.InnerRadius:        redraw = ApplyRadiusChange(angleRoi, Radius.Inner);                         break;                
                case ResizeType.OutterRadius:       redraw = ApplyRadiusChange(angleRoi, Radius.Outer);                         break;
                case ResizeType.ThetaStart:         redraw = ApplyAngleChange(angleRoi, Mouse.GetPosition(this), Theta.Start);  break;
                case ResizeType.ThetaEnd:           redraw = ApplyAngleChange(angleRoi, Mouse.GetPosition(this), Theta.End);    break;
                default:
                    return;
            }

            //so the object gets re-drawn
            if (redraw)
            {
                angleRoi.DrawShape();
            }
        }

        private bool ApplyRadiusChange(PointerAngleZone angleRoi, Radius radius)
        {
            var mousePos = Mouse.GetPosition(this);
            double distanceToMouse = GeometryUtilities.GetDistanceBetweenPoints(mousePos, angleRoi.CenterCoord.Center);
            double angle = GeometryUtilities.ConvertToRadians(GeometryUtilities.GetAngleOnYaxisInverted(angleRoi.CenterCoord.Center, Mouse.GetPosition(this)));

            //set the Arrow cursor
            Cursor = CursorUtilities.GetCursorFromRadius(GeometryUtilities.ConvertToDegrees(angle));

            return angleRoi.ExtendRadius(distanceToMouse, radius);
        }

        private bool ApplyAngleChange(PointerAngleZone angleRoi, Point actualPosition, Theta selectedAngle)
        {
            double angle = GeometryUtilities.GetAngleOnYaxisInverted(angleRoi.CenterCoord.Center, actualPosition);

            //set the Arrow cursor
            Cursor = CursorUtilities.GetCursorFromAngle(angle);

            return angleRoi.SetAngle(angle, selectedAngle);
        }

        /// <summary>
        /// Applies a delta constraint to the specified DesignerItemModelBase item
        /// </summary>
        /// <param name="item">item to be resized</param>
        /// <param name="e">delta args</param>
        private void ApplyRectangleChange(RectangularZone item, DragDeltaEventArgs e)
        {            
            double deltaVertical = Math.Max((double.IsNaN(item.Top) ? 0 : -item.Top), e.VerticalChange);
            double deltaHorizontal = Math.Max((double.IsNaN(item.Left) ? 0 : -item.Left), e.HorizontalChange);
            bool evenly = Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl);
            deltaVertical = Math.Min(deltaVertical, item.ItemHeight - _minHeight);
            deltaHorizontal = Math.Min(deltaHorizontal, item.ItemWidth - _minWidth);

            switch (VerticalAlignment)
            {
                case VerticalAlignment.Top:
                    item.Top += deltaVertical;
                    item.ItemHeight -= deltaVertical;
                    break;

                case VerticalAlignment.Bottom:
                    item.ItemHeight += Math.Max(e.VerticalChange, _minHeight - item.ItemHeight);
                    break;
            }

            switch (HorizontalAlignment)
            {
                case HorizontalAlignment.Left:
                    item.Left += deltaHorizontal;
                    item.ItemWidth -= deltaHorizontal;
                    break;

                case HorizontalAlignment.Right:
                    item.ItemWidth += Math.Max(e.HorizontalChange, _minWidth - item.ItemWidth);
                    break;
            }

            if (evenly)
            {
                if (VerticalAlignment == VerticalAlignment.Bottom)
                {
                    if (HorizontalAlignment == HorizontalAlignment.Left || HorizontalAlignment == HorizontalAlignment.Right)
                    {
                        item.ItemHeight = item.ItemWidth;
                    }
                }
                else if (VerticalAlignment == VerticalAlignment.Top && HorizontalAlignment != HorizontalAlignment.Left)
                {
                    item.ItemWidth = item.ItemHeight;
                }
                else
                {
                    item.Top -= deltaVertical;
                    item.ItemHeight += deltaVertical;


                    item.Top += deltaHorizontal;
                    item.ItemHeight = item.ItemWidth;
                }
            }

            item.DrawShape();
        }
    }
}
