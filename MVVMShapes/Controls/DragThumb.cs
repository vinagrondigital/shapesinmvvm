﻿using MVVMShapes.Interfaces;
using MVVMShapes.Models;
using System;
using System.Windows.Controls.Primitives;

namespace MVVMShapes.Controls
{
    /// <summary>
    /// DragThumb iDEA control, used for moving objects inside a iDEADesignerCanvas
    /// </summary>
    public class DragThumb : Thumb
    {
        public DragThumb()
        {
            DragDelta += new DragDeltaEventHandler(OnDragDelta);
        }

        /// <summary>
        /// DragDelta hook event, computations based on: https://stackoverflow.com/questions/26298398/wpf-thumb-drag-behaviour-wrong
        /// Handles object movement with mouse
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnDragDelta(object sender, DragDeltaEventArgs e)
        {
            //TODO: add right/bottom boundaries
            if (DataContext is DesignerItemModelBase item)
            {
                IImageEditor editor = item.Parent;

                double minLeft = double.MaxValue;
                double minTop = double.MaxValue;

                double left = item.Left;
                double top = item.Top;

                minLeft = double.IsNaN(left) ? 0 : Math.Min(left, minLeft);
                minTop = double.IsNaN(top) ? 0 : Math.Min(top, minTop);

                double deltaHorizontal = Math.Max(-minLeft, e.HorizontalChange);
                double deltaVertical = Math.Max(-minTop, e.VerticalChange);

                //validate horizontal X positive boundary
                if(e.HorizontalChange > 0)
                {
                    if((item.Left + item.ItemWidth + e.HorizontalChange) >= editor.CanvasSize.X)
                    {
                        deltaHorizontal = 0;
                        item.Left = editor.CanvasSize.X - item.ItemWidth;
                    }
                }

                //validate vertical Y positive boundary
                if (e.VerticalChange> 0)
                {
                    if ((item.Top + item.ItemHeight + e.VerticalChange) >= editor.CanvasSize.Y)
                    {
                        deltaVertical = 0;
                        item.Top = editor.CanvasSize.Y - item.ItemHeight;
                    }
                }

                //add the new coordinates
                if (item.Parent != null && item.Parent.Items != null)
                {
                    foreach (var obj in item.Parent.Items)
                    {
                        if (obj.IsSelected == true)
                        {
                            obj.Left += deltaHorizontal;
                            obj.Top += deltaVertical;
                        }
                    }
                }

                //todo: maybe add single item movement if parent is null?

                e.Handled = true;
            }
        }
    }
}
