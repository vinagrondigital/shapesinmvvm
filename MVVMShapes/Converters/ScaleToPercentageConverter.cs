﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace MVVMShapes.Converters
{
    /// <summary>
    /// Converts a scale (1.0) to percentage (100%)
    /// </summary>
    public class ScaleToPercentageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is double scale)
            {
                if (double.IsNaN(scale))
                {
                    return "? %";
                }

                return $"{Math.Round(scale * 100, 2)}%";
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return new NotImplementedException();
        }
    }
}
