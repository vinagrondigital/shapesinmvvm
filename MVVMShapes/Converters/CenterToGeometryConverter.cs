﻿using MVVMShapes.SearchZones;
using MVVMShapes.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace MVVMShapes.Converters
{
    public class CenterToGeometryConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value is ConverterContext zone)
            {
                List<PathSegment> segments = new List<PathSegment>();
                bool isBigArc = GeometryUtilities.IsBigArc(zone.CenterCoord.ThetaStart, zone.CenterCoord.ThetaEnd);
                bool success = bool.TryParse(parameter as string, out bool isInnerRadius);
                Point start;

                if (!success || isInnerRadius)
                {
                    start = zone.MyPoints[0];                    
                    segments.Add(new ArcSegment(zone.MyPoints[1], new Size(zone.CenterCoord.InnerRadius, zone.CenterCoord.InnerRadius), 0, isBigArc, SweepDirection.Counterclockwise, true));
                }
                else
                {
                    start = zone.MyPoints[2];
                    segments.Add(new ArcSegment(zone.MyPoints[3], new Size(zone.CenterCoord.OuterRadius, zone.CenterCoord.OuterRadius), 0, isBigArc, SweepDirection.Clockwise, true));
                }

                PathFigure figure = new PathFigure(start, segments, false);
                var geometry = new PathGeometry();
                geometry.Figures.Add(figure);

                return geometry;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
