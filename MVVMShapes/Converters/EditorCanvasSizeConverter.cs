﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace MVVMShapes.Converters
{
    public class EditorCanvasSizeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Point canvasSize)
            {
                return $"{canvasSize.X}x{canvasSize.Y}";
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
