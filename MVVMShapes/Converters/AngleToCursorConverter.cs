﻿using MVVMShapes.Utilities;
using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Input;

namespace MVVMShapes.Converters
{
    public class AngleToCursorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is double angle)
            {

                bool success = bool.TryParse(parameter as string, out bool useForRadius);


                if (!success)
                {
                    return CursorUtilities.GetCursorFromAngle(angle);
                }
                else
                {
                    if (useForRadius)
                    {
                        return CursorUtilities.GetCursorFromRadius(angle);
                    }
                    else
                    {
                        return CursorUtilities.GetCursorFromAngle(angle);
                    }
                }
            }

            return Cursors.Arrow;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
