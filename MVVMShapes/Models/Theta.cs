﻿namespace MVVMShapes.Models
{
    public enum Theta : byte
    {
        Start = 1,
        End   = 2,
    }
}
