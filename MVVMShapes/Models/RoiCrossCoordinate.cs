﻿using Prism.Mvvm;
using System.Windows;
using System.Windows.Shapes;

namespace MVVMShapes.Models
{
    /// <summary>
    /// Abstracts for the cross line
    /// </summary>
    public class RoiCrossCoordinate : BindableBase
    {
        #region Bindable base
        private double _x1;
        public double X1
        {
            get { return _x1; }
            set { SetProperty(ref _x1, value); }
        }

        private double _x2;
        public double X2
        {
            get { return _x2; }
            set { SetProperty(ref _x2, value); }
        }


        private double _y1;
        public double Y1
        {
            get { return _y1; }
            set { SetProperty(ref _y1, value); }
        }

        private double _y2;
        public double Y2
        {
            get { return _y2; }
            set { SetProperty(ref _y2, value); }
        }
        #endregion

        public double MaxSize { get; set; }
        public bool IsHorizontal { get; set; }
        public double SizePercentage { get; set; }

        public RoiCrossCoordinate()
        {
            X1 = X2 = Y1 = Y2 = 0;
            IsHorizontal = false;
        }

        /// <summary>
        /// Ctor, creates the line coordinate
        /// </summary>
        /// <param name="width">object width</param>
        /// <param name="height">object height</param>
        /// <param name="isHorizontal">ishorizontal line</param>
        /// <param name="maxSize">max allowed size</param>
        /// <param name="sizePercentage">percentage of size from width/height</param>
        public RoiCrossCoordinate(double width, double height, bool isHorizontal, double maxSize = 10, double sizePercentage = 10)
        {
            MaxSize = maxSize;
            IsHorizontal = isHorizontal;
            SizePercentage = sizePercentage;

            //Sanity checks
            if (SizePercentage > 100)
            {
                SizePercentage = 100;
            }

            if (SizePercentage < 0)
            {
                SizePercentage = 0;
            }

            Calculate(width, height, isHorizontal);
        }
    }
}
