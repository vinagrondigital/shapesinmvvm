﻿using System;
using System.Windows;
using System.Collections.Generic;

using MVVMShapes.Utilities;

namespace MVVMShapes.Models
{
    public class BoundingBox
    {
        public double Top { get; private set; }
        public double Left { get; private set; }
        public double Right { get; private set; }
        public double Bottom { get; private set; }

        public double Width => Right - Left;
        public double Height => Bottom - Top;

        public BoundingBox(double top, double left, double right, double bottom)
        {
            Top    = top;
            Left   = left;
            Right  = right;
            Bottom = bottom;
        }

        public List<Point> GetBoundingPoints()
        {
            return new List<Point>()
            {
                new Point(Top   , Left ),
                new Point(Top   , Right),
                new Point(Bottom, Left ),
                new Point(Bottom, Right),
            };
        }

        public void IncludePoint(Point point)
        {
            if(point == null)
            {
                throw new ArgumentNullException(nameof(point));
            }

            //include point in X axis
            if (point.X > Right)
            {
                //point has greater value than the limit box range in the X axis, extend right value
                Right = point.X;
            }
            else if(point.X < Left)
            {
                //point has lower value than the limit box range in the X axis, extend left value
                Left = point.X;
            }

            if (point.Y > Bottom)
            {
                //point has greater value than the limit box range in the Y axis, extend bottom value
                Bottom = point.Y;
            }
            else if (point.Y < Top)
            {
                //point has lower value than the limit box range in the Y axis, extend top value
                Top = point.Y;
            }
        }

        public void AddMargin(double margin)
        {
            Top    -= margin;
            Left   -= margin;
            Right  += margin;
            Bottom += margin;
        }

        public static BoundingBox CreateNew(PointerAngleCoordinate arcDefinition, bool includeCenter)
        {
            if (arcDefinition == null)
            {
                throw new ArgumentNullException(nameof(arcDefinition));
            }

            var pointsDefinition = arcDefinition.GetCoordinateList();

            var limitBox = new BoundingBox(ArcUtilities.GetPointerAngleLimit(arcDefinition, pointsDefinition, Limit.Top),
                                           ArcUtilities.GetPointerAngleLimit(arcDefinition, pointsDefinition, Limit.Left),
                                           ArcUtilities.GetPointerAngleLimit(arcDefinition, pointsDefinition, Limit.Right),
                                           ArcUtilities.GetPointerAngleLimit(arcDefinition, pointsDefinition, Limit.Bottom));

            if(includeCenter)
            {
                limitBox.IncludePoint(arcDefinition.Center);
            }

            return limitBox;
        }
    }
}
