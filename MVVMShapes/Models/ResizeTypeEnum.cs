﻿namespace MVVMShapes.Models
{
    public enum ResizeType
    {
        Default,
        OutterRadius,
        OutterRadiusWithMouse,
        InnerRadius,
        InnerRadiusWithMouse,
        Width,
        Height,
        ThetaStart,
        ThetaEnd,
        Angle,
    }
}
