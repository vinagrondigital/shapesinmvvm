﻿using MVVMShapes.Utilities;
using System;
using System.Collections.Generic;
using System.Windows;

namespace MVVMShapes.Models
{
    /// <summary>
    /// Abstraction model for a pointer angle center coordinate
    /// </summary>
    public class PointerAngleCoordinate
    {
        /// <summary>
        /// Center point
        /// </summary>
        public Point Center { get; set; }

        /// <summary>
        /// Inner circle radius (R1)
        /// </summary>
        public double InnerRadius { get; set; }

        /// <summary>
        /// Outer cicrle radius (R2)
        /// </summary>
        public double OuterRadius { get; set; }

        /// <summary>
        /// Start angle (0-360)
        /// </summary>
        public double ThetaStart { get; set; }


        public double ThetaStartRadians => (ThetaStart * Math.PI) / 180.0;

        /// <summary>
        /// End angle (0-360)
        /// </summary>
        public double ThetaEnd { get; set; }
        public double ThetaEndRadians => (ThetaEnd * Math.PI) / 180.0;

        public double ArcDifference => ThetaEnd - ThetaStart;
        public double ArcLength => OuterRadius - InnerRadius;

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="centerCoord">Start coordinate</param>
        /// <param name="relativeX">offset for X, will be substracted</param>
        /// <param name="relativeY">offset for Y, will be substracted</param>
        public PointerAngleCoordinate(DBCoordinate centerCoord, double relativeX = 0, double relativeY = 0)
        {
            //sanitize this
            if (centerCoord == null)
            {
                throw new ArgumentNullException(nameof(centerCoord));
            }

            if (centerCoord.R1.HasValue == false)
            {
                throw new InvalidOperationException($"Provided CenterR1 is null");
            }

            if (centerCoord.R2.HasValue == false)
            {
                throw new InvalidOperationException($"Provided CenterR1 is null");
            }

            if (centerCoord.Theta_start.HasValue == false)
            {
                throw new InvalidOperationException($"Provided Theta_start is null");
            }

            if (centerCoord.Theta_end.HasValue == false)
            {
                throw new InvalidOperationException($"Provided Theta_end is null");
            }

            //add data
            Center = new Point()
            {
                X = centerCoord.X - relativeX,
                Y = centerCoord.Y - relativeY
            };
            InnerRadius = Math.Min((double)centerCoord.R1, (double)centerCoord.R2);
            OuterRadius = Math.Max((double)centerCoord.R1, (double)centerCoord.R2);
            ThetaStart = (double)centerCoord.Theta_start;
            ThetaEnd = (double)centerCoord.Theta_end;
        }

        /// <summary>
        ///     Gets the list of the coordinates that form the shape.
        /// </summary>
        /// <returns></returns>
        public List<Point> GetCoordinateList()
        {
            return new List<Point>()
            {
                GeometryUtilities.GetCirclePoint(Center, InnerRadius, ThetaStart),
                GeometryUtilities.GetCirclePoint(Center, InnerRadius, ThetaEnd),
                GeometryUtilities.GetCirclePoint(Center, OuterRadius, ThetaEnd),
                GeometryUtilities.GetCirclePoint(Center, OuterRadius, ThetaStart),
                GeometryUtilities.GetCirclePoint(Center, InnerRadius, ThetaStart),
            };
        }

        /// <summary>
        ///     Sets the angle in degrees for angle (thetaStart or thetaEnd).
        /// </summary>
        /// <param name="angleInDegrees"></param>
        public void SetAngle(double angleInDegrees, Theta selectedAngle)
        {
            switch (selectedAngle)
            {
                case Theta.Start:
                    ThetaStart = angleInDegrees;
                    break;
                case Theta.End:
                    ThetaEnd = angleInDegrees;
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary>
        ///     Readjust the arc to a miminum angle.
        /// </summary>
        /// <param name="minAngleInDegrees"></param>
        public void ReadjustWithMinimum(double minAngleInDegrees, Theta selectedAngle)
        {
            if (ArcDifference > 0 && ArcDifference < minAngleInDegrees)
            {
                switch (selectedAngle)
                {
                    case Theta.Start:
                        ThetaStart -= (minAngleInDegrees - ArcDifference);
                        break;
                    case Theta.End:
                        ThetaEnd += (minAngleInDegrees - ArcDifference);
                        break;
                    default:
                        throw new NotImplementedException();
                }
            }
        }

        public Point GetArcMiddlePoint()
        {
            double middleRadius = (InnerRadius + OuterRadius) / 2;
            double middleAngle  = (ThetaEnd + ThetaStart) / 2;

            if(ThetaStart > ThetaEnd)
            {
                // substract half circle to position it correctly
                middleAngle -= 180.0;
            }

            return GeometryUtilities.GetCirclePoint(Center, middleRadius, middleAngle);
        }
    }
}
