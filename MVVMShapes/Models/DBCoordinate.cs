﻿namespace MVVMShapes.Models
{
    public class DBCoordinate
    {
        public int id { get; set; }
        public int Search_zone_id { get; set; }
        public bool Is_fiducial { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
        public double? R1 { get; set; }
        public double? R2 { get; set; }
        public double? Theta_start { get; set; }
        public double? Theta_end { get; set; }
    }
}
