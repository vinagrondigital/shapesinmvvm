﻿namespace MVVMShapes.Models
{
    public enum Axis : byte
    {
        X = 0,
        Y = 1,
    }
}
