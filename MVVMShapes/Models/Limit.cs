﻿namespace MVVMShapes.Models
{
    public enum Limit
    {
        Top    = 0,
        Left   = 1,
        Right  = 2,
        Bottom = 3,
    }
}
