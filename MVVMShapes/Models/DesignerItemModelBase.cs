﻿using MVVMShapes.Interfaces;
using Prism.Mvvm;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Media;

namespace MVVMShapes.Models
{
    /// <summary>
    /// Designer item base class, implemets IEditorRoi, IDesignItem
    /// </summary>
    public abstract class DesignerItemModelBase : BindableBase, IEditorRoi, IDesignItem
    {
        #region Bindable (IEditorRoi, IDesignItem) stuff

        private ObservableCollection<Point> _myPoints;
        public ObservableCollection<Point> MyPoints
        {
            get { return _myPoints; }
            set { SetProperty(ref _myPoints, value); }
        }

        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set { SetProperty(ref _isSelected, value); }
        }

        private int _id;
        public int Id
        {
            get { return _id; }
            set { SetProperty(ref _id, value); }
        }

        private double _left;
        public double Left
        {
            get { return _left; }
            set { SetProperty(ref _left, value); }
        }

        private double _top;
        public double Top
        {
            get { return _top; }
            set { SetProperty(ref _top, value); }
        }

        private double _itemWidth;
        public double ItemWidth
        {
            get { return _itemWidth; }
            set { SetProperty(ref _itemWidth, value); }
        }

        private double _itemHeight;
        public double ItemHeight
        {
            get { return _itemHeight; }
            set { SetProperty(ref _itemHeight, value); }
        }

        private DesignerItemType _roiType;
        public DesignerItemType RoiType
        {
            get { return _roiType; }
            set { SetProperty(ref _roiType, value); }
        }

        public double Right => Left + ItemWidth;
        public double Bottom => Top + ItemHeight;

        private ObservableCollection<DBCoordinate> _coordinates;
        public ObservableCollection<DBCoordinate> Coordinates
        {
            get { return _coordinates; }
            set { SetProperty(ref _coordinates, value); }
        }


        public IImageEditor Parent { get; set; }
        public DesignerItemConstraint Constraint { get; set; }
        #endregion

        public double MinWidth { get; set; }
        public double MinHeight { get; set; }

        #region Ctors       
        public DesignerItemModelBase(int id, double left, double top, DesignerItemType type = DesignerItemType.RectangularROI, IImageEditor parent = null)
        {
            //set default values
            Initialize();

            //set the new properties
            Id = id;
            Left = left;
            Top = top;
            RoiType = type;
            Parent = parent;
        }

        public DesignerItemModelBase(DesignerItemType roiType, IImageEditor parent)
        {
            Initialize();
            RoiType = roiType;
            Parent = parent;
        }
        #endregion

        public DesignerItemModelBase()
        {
            Initialize();
        }

        public DesignerItemModelBase(DesignerItemType type)
        {
            Initialize();

            RoiType = type;
        }

        protected void SetMinWidthHeight(double minWidth, double minHeight)
        {
            MinWidth = minWidth;
            MinHeight = minHeight;
        }

        #region Private functions
        private void Initialize()
        {
            Id = 0;
            Left = Top = 0;
            ItemWidth = 100;
            ItemHeight = 25;
            MinWidth = MinHeight = 10;
            IsSelected = false;
            RoiType = DesignerItemType.RectangularROI;
            Coordinates = new ObservableCollection<DBCoordinate>();
            Constraint = DesignerItemConstraint.None;
        }

        public void Select()
        {
            if (RoiType != DesignerItemType.Bitmap)
            {
                IsSelected = true;
            }
        }

        public void Deselect() => IsSelected = false;
        #endregion
    }
}
