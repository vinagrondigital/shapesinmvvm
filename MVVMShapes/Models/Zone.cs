﻿using System.Windows.Media;

using MVVMShapes.Utilities;
using MVVMShapes.Interfaces;

namespace MVVMShapes.Models
{
    public abstract class Zone : DesignerItemModelBase, IZone
    {
        public Zone(DesignerItemType roiType, IImageEditor parent) 
            : base(roiType, parent)
        {
            //nothing here
        }

        public Zone(int id, double left, double top, DesignerItemType type = DesignerItemType.RectangularROI, IImageEditor parent = null) 
            : base(id, left, top, type, parent)
        {
            //nothing here
        }

        private GeometryGroup _zoneGeometry;
        public GeometryGroup ZoneGeometry
        {
            get { return _zoneGeometry; }
            set { SetProperty(ref _zoneGeometry, value); }
        }

        public virtual void DrawShape()
        {
            //create the bindable geometry
            ZoneGeometry = PathUtilities.CreateZoneGeometry(this);
        }
    }
}
