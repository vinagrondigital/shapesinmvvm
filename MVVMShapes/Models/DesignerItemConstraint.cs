﻿namespace MVVMShapes.Models
{
    public enum DesignerItemConstraint
    {
        None,
        ResizeEvenly,
        ResizeVerticallyOnly,
        ResizeHorizontallyOnly,
    }
}
