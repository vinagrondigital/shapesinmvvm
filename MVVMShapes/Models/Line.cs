﻿using System.Windows;

namespace MVVMShapes.Models
{
    public class Line
    {
        public double X1 { get; private set; }
        public double X2 { get; private set; }
        public double Y1 { get; private set; }
        public double Y2 { get; private set; }

        public double Width => X2 - X1;
        public double Height => Y2 - Y1;

        public Point GetStartPoint()
        {
            return new Point(X1, Y1);
        }

        public Point GetDestPoint()
        {
            return new Point(X2, Y2);
        }

        public Line(double x1, double y1, double x2, double y2)
        {
            X1 = x1;
            Y1 = y1;

            X2 = x2;
            Y2 = y2;
        }

        public Line(Point start, Point end)
        {
            X1 = start.X;
            Y1 = start.Y;

            X2 = end.X;
            Y2 = end.Y;
        }
    }
}
