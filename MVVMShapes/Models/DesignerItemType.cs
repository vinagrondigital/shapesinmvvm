﻿namespace MVVMShapes.Models
{
    /// <summary>
    /// Designer item type abstraction enum
    /// </summary>
    public enum DesignerItemType
    {
        RectangularROI,
        PointerAngleROI,
        Bitmap,
    }
}
